﻿namespace BinduraRegistration
{
    partial class frmRegister
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabPersonalDetails = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.cmbConsiderForResidence = new System.Windows.Forms.ComboBox();
            this.label70 = new System.Windows.Forms.Label();
            this.cmbResidentStatus = new System.Windows.Forms.ComboBox();
            this.dtpDOB = new System.Windows.Forms.DateTimePicker();
            this.cmbSex = new System.Windows.Forms.ComboBox();
            this.cmbTitle = new System.Windows.Forms.ComboBox();
            this.txtDisabilities = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.txtPassportCountry = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.txtPassportNo = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.txtResidencePeriod = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.txtPermit = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.txtPostalAddress = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.txtCitizenship = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtNationalID = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtPlaceOfBirth = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txtPreviousSurname = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtSurname = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtFirstName = new System.Windows.Forms.TextBox();
            this.lblSurname = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.txtKinEmail = new System.Windows.Forms.TextBox();
            this.label27 = new System.Windows.Forms.Label();
            this.txtKinContacts = new System.Windows.Forms.TextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.txtKinPostalAddress = new System.Windows.Forms.TextBox();
            this.label25 = new System.Windows.Forms.Label();
            this.txtKinSurname = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.txtKinFirstName = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.btnClearExam = new System.Windows.Forms.Button();
            this.btnExamRemove = new System.Windows.Forms.Button();
            this.btmExamAdd = new System.Windows.Forms.Button();
            this.txtExamResult = new System.Windows.Forms.TextBox();
            this.label32 = new System.Windows.Forms.Label();
            this.txtExamSubject = new System.Windows.Forms.TextBox();
            this.label31 = new System.Windows.Forms.Label();
            this.txtExamLevel = new System.Windows.Forms.TextBox();
            this.label30 = new System.Windows.Forms.Label();
            this.txtExamBoard = new System.Windows.Forms.TextBox();
            this.label29 = new System.Windows.Forms.Label();
            this.txtExamDate = new System.Windows.Forms.TextBox();
            this.label28 = new System.Windows.Forms.Label();
            this.lvwExams = new System.Windows.Forms.ListView();
            this.label18 = new System.Windows.Forms.Label();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.cmbLevel = new System.Windows.Forms.ComboBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label38 = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.txtProgram2Option2 = new System.Windows.Forms.TextBox();
            this.txtProgram2Option1 = new System.Windows.Forms.TextBox();
            this.label40 = new System.Windows.Forms.Label();
            this.txtProgram2Name = new System.Windows.Forms.TextBox();
            this.label41 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.label42 = new System.Windows.Forms.Label();
            this.label43 = new System.Windows.Forms.Label();
            this.txtProgram3Option2 = new System.Windows.Forms.TextBox();
            this.txtProgram3Option1 = new System.Windows.Forms.TextBox();
            this.label44 = new System.Windows.Forms.Label();
            this.txtProgram3Name = new System.Windows.Forms.TextBox();
            this.label45 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label36 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.txtProgram1Option2 = new System.Windows.Forms.TextBox();
            this.txtProgram1Option1 = new System.Windows.Forms.TextBox();
            this.label34 = new System.Windows.Forms.Label();
            this.txtProgram1Name = new System.Windows.Forms.TextBox();
            this.label33 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.txtReference2 = new System.Windows.Forms.TextBox();
            this.label53 = new System.Windows.Forms.Label();
            this.txtReference1 = new System.Windows.Forms.TextBox();
            this.label52 = new System.Windows.Forms.Label();
            this.txtNoticePeriod = new System.Windows.Forms.TextBox();
            this.label51 = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.txtCurrentEmployer = new System.Windows.Forms.TextBox();
            this.label46 = new System.Windows.Forms.Label();
            this.dtpEmpTo = new System.Windows.Forms.DateTimePicker();
            this.label49 = new System.Windows.Forms.Label();
            this.dtpEmpFrom = new System.Windows.Forms.DateTimePicker();
            this.btnEmpRemove = new System.Windows.Forms.Button();
            this.btnEmpAdd = new System.Windows.Forms.Button();
            this.txtDuties = new System.Windows.Forms.TextBox();
            this.label47 = new System.Windows.Forms.Label();
            this.txtEmployer = new System.Windows.Forms.TextBox();
            this.label48 = new System.Windows.Forms.Label();
            this.label50 = new System.Windows.Forms.Label();
            this.lvwEmployment = new System.Windows.Forms.ListView();
            this.label20 = new System.Windows.Forms.Label();
            this.tabPage6 = new System.Windows.Forms.TabPage();
            this.txtEmployeeNo = new System.Windows.Forms.TextBox();
            this.label59 = new System.Windows.Forms.Label();
            this.txtStaffDepartment = new System.Windows.Forms.TextBox();
            this.label58 = new System.Windows.Forms.Label();
            this.cmbIsStaff = new System.Windows.Forms.ComboBox();
            this.label56 = new System.Windows.Forms.Label();
            this.cmbIsStaffDependant = new System.Windows.Forms.ComboBox();
            this.label55 = new System.Windows.Forms.Label();
            this.txtSponsorName = new System.Windows.Forms.TextBox();
            this.label54 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.tabPage7 = new System.Windows.Forms.TabPage();
            this.cmbHasBirthCertificate = new System.Windows.Forms.ComboBox();
            this.cmbHasTranscript = new System.Windows.Forms.ComboBox();
            this.cmbHasCertificate = new System.Windows.Forms.ComboBox();
            this.cmbHasDiplomaOrDegree = new System.Windows.Forms.ComboBox();
            this.cmbHasALevel = new System.Windows.Forms.ComboBox();
            this.cmbHasMaths = new System.Windows.Forms.ComboBox();
            this.cmbHasEnglish = new System.Windows.Forms.ComboBox();
            this.cmbHasFiveOLevels = new System.Windows.Forms.ComboBox();
            this.label69 = new System.Windows.Forms.Label();
            this.label68 = new System.Windows.Forms.Label();
            this.txtOtherCertificates = new System.Windows.Forms.TextBox();
            this.label67 = new System.Windows.Forms.Label();
            this.label66 = new System.Windows.Forms.Label();
            this.label65 = new System.Windows.Forms.Label();
            this.label64 = new System.Windows.Forms.Label();
            this.label63 = new System.Windows.Forms.Label();
            this.label62 = new System.Windows.Forms.Label();
            this.label61 = new System.Windows.Forms.Label();
            this.cmbHasIdentification = new System.Windows.Forms.ComboBox();
            this.label60 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.btnSave = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.btnClear = new System.Windows.Forms.Button();
            this.txtEmail = new System.Windows.Forms.TextBox();
            this.label71 = new System.Windows.Forms.Label();
            this.txtPhone = new System.Windows.Forms.TextBox();
            this.label72 = new System.Windows.Forms.Label();
            this.txtId = new System.Windows.Forms.TextBox();
            this.tabPersonalDetails.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.tabPage4.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.tabPage5.SuspendLayout();
            this.tabPage6.SuspendLayout();
            this.tabPage7.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabPersonalDetails
            // 
            this.tabPersonalDetails.Controls.Add(this.tabPage1);
            this.tabPersonalDetails.Controls.Add(this.tabPage2);
            this.tabPersonalDetails.Controls.Add(this.tabPage3);
            this.tabPersonalDetails.Controls.Add(this.tabPage4);
            this.tabPersonalDetails.Controls.Add(this.tabPage5);
            this.tabPersonalDetails.Controls.Add(this.tabPage6);
            this.tabPersonalDetails.Controls.Add(this.tabPage7);
            this.tabPersonalDetails.Location = new System.Drawing.Point(21, 12);
            this.tabPersonalDetails.Name = "tabPersonalDetails";
            this.tabPersonalDetails.SelectedIndex = 0;
            this.tabPersonalDetails.Size = new System.Drawing.Size(762, 528);
            this.tabPersonalDetails.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.txtPhone);
            this.tabPage1.Controls.Add(this.label72);
            this.tabPage1.Controls.Add(this.txtEmail);
            this.tabPage1.Controls.Add(this.label71);
            this.tabPage1.Controls.Add(this.cmbConsiderForResidence);
            this.tabPage1.Controls.Add(this.label70);
            this.tabPage1.Controls.Add(this.cmbResidentStatus);
            this.tabPage1.Controls.Add(this.dtpDOB);
            this.tabPage1.Controls.Add(this.cmbSex);
            this.tabPage1.Controls.Add(this.cmbTitle);
            this.tabPage1.Controls.Add(this.txtDisabilities);
            this.tabPage1.Controls.Add(this.label16);
            this.tabPage1.Controls.Add(this.label15);
            this.tabPage1.Controls.Add(this.txtPassportCountry);
            this.tabPage1.Controls.Add(this.label14);
            this.tabPage1.Controls.Add(this.txtPassportNo);
            this.tabPage1.Controls.Add(this.label13);
            this.tabPage1.Controls.Add(this.txtResidencePeriod);
            this.tabPage1.Controls.Add(this.label12);
            this.tabPage1.Controls.Add(this.txtPermit);
            this.tabPage1.Controls.Add(this.label11);
            this.tabPage1.Controls.Add(this.txtPostalAddress);
            this.tabPage1.Controls.Add(this.label1);
            this.tabPage1.Controls.Add(this.label10);
            this.tabPage1.Controls.Add(this.txtCitizenship);
            this.tabPage1.Controls.Add(this.label9);
            this.tabPage1.Controls.Add(this.txtNationalID);
            this.tabPage1.Controls.Add(this.label8);
            this.tabPage1.Controls.Add(this.txtPlaceOfBirth);
            this.tabPage1.Controls.Add(this.label7);
            this.tabPage1.Controls.Add(this.label6);
            this.tabPage1.Controls.Add(this.label5);
            this.tabPage1.Controls.Add(this.txtPreviousSurname);
            this.tabPage1.Controls.Add(this.label4);
            this.tabPage1.Controls.Add(this.label3);
            this.tabPage1.Controls.Add(this.txtSurname);
            this.tabPage1.Controls.Add(this.label2);
            this.tabPage1.Controls.Add(this.txtFirstName);
            this.tabPage1.Controls.Add(this.lblSurname);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(754, 502);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Personal Details";
            this.tabPage1.UseVisualStyleBackColor = true;
            this.tabPage1.Click += new System.EventHandler(this.tabPage1_Click);
            // 
            // cmbConsiderForResidence
            // 
            this.cmbConsiderForResidence.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbConsiderForResidence.FormattingEnabled = true;
            this.cmbConsiderForResidence.Items.AddRange(new object[] {
            "Yes",
            "No"});
            this.cmbConsiderForResidence.Location = new System.Drawing.Point(184, 428);
            this.cmbConsiderForResidence.Name = "cmbConsiderForResidence";
            this.cmbConsiderForResidence.Size = new System.Drawing.Size(124, 21);
            this.cmbConsiderForResidence.TabIndex = 31;
            // 
            // label70
            // 
            this.label70.AutoSize = true;
            this.label70.Location = new System.Drawing.Point(406, 328);
            this.label70.Name = "label70";
            this.label70.Size = new System.Drawing.Size(36, 13);
            this.label70.TabIndex = 23;
            this.label70.Text = "Permit";
            // 
            // cmbResidentStatus
            // 
            this.cmbResidentStatus.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbResidentStatus.FormattingEnabled = true;
            this.cmbResidentStatus.Items.AddRange(new object[] {
            "Permanent",
            "Other"});
            this.cmbResidentStatus.Location = new System.Drawing.Point(184, 319);
            this.cmbResidentStatus.Name = "cmbResidentStatus";
            this.cmbResidentStatus.Size = new System.Drawing.Size(141, 21);
            this.cmbResidentStatus.TabIndex = 23;
            // 
            // dtpDOB
            // 
            this.dtpDOB.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpDOB.Location = new System.Drawing.Point(184, 177);
            this.dtpDOB.Name = "dtpDOB";
            this.dtpDOB.Size = new System.Drawing.Size(200, 20);
            this.dtpDOB.TabIndex = 13;
            // 
            // cmbSex
            // 
            this.cmbSex.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbSex.FormattingEnabled = true;
            this.cmbSex.Items.AddRange(new object[] {
            "Female",
            "Male"});
            this.cmbSex.Location = new System.Drawing.Point(552, 65);
            this.cmbSex.Name = "cmbSex";
            this.cmbSex.Size = new System.Drawing.Size(82, 21);
            this.cmbSex.TabIndex = 5;
            // 
            // cmbTitle
            // 
            this.cmbTitle.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbTitle.FormattingEnabled = true;
            this.cmbTitle.Items.AddRange(new object[] {
            "Mr",
            "Mrs",
            "Miss",
            "Ms",
            "Dr",
            "Prof"});
            this.cmbTitle.Location = new System.Drawing.Point(185, 62);
            this.cmbTitle.Name = "cmbTitle";
            this.cmbTitle.Size = new System.Drawing.Size(82, 21);
            this.cmbTitle.TabIndex = 3;
            // 
            // txtDisabilities
            // 
            this.txtDisabilities.Location = new System.Drawing.Point(184, 465);
            this.txtDisabilities.Name = "txtDisabilities";
            this.txtDisabilities.Size = new System.Drawing.Size(542, 20);
            this.txtDisabilities.TabIndex = 33;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(73, 465);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(90, 13);
            this.label16.TabIndex = 32;
            this.label16.Text = "Disabilities (if any)";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(46, 431);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(117, 13);
            this.label15.TabIndex = 30;
            this.label15.Text = "Consider for Residence";
            // 
            // txtPassportCountry
            // 
            this.txtPassportCountry.Location = new System.Drawing.Point(554, 393);
            this.txtPassportCountry.Name = "txtPassportCountry";
            this.txtPassportCountry.Size = new System.Drawing.Size(172, 20);
            this.txtPassportCountry.TabIndex = 29;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(421, 396);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(115, 13);
            this.label14.TabIndex = 28;
            this.label14.Text = "Place/Country of Issue";
            // 
            // txtPassportNo
            // 
            this.txtPassportNo.Location = new System.Drawing.Point(184, 393);
            this.txtPassportNo.Name = "txtPassportNo";
            this.txtPassportNo.Size = new System.Drawing.Size(172, 20);
            this.txtPassportNo.TabIndex = 27;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(95, 396);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(68, 13);
            this.label13.TabIndex = 26;
            this.label13.Text = "Passport No.";
            // 
            // txtResidencePeriod
            // 
            this.txtResidencePeriod.Location = new System.Drawing.Point(184, 359);
            this.txtResidencePeriod.Name = "txtResidencePeriod";
            this.txtResidencePeriod.Size = new System.Drawing.Size(172, 20);
            this.txtResidencePeriod.TabIndex = 25;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(60, 362);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(103, 13);
            this.label12.TabIndex = 24;
            this.label12.Text = "Period of Residence";
            // 
            // txtPermit
            // 
            this.txtPermit.Location = new System.Drawing.Point(465, 325);
            this.txtPermit.Name = "txtPermit";
            this.txtPermit.Size = new System.Drawing.Size(262, 20);
            this.txtPermit.TabIndex = 23;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(81, 327);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(82, 13);
            this.label11.TabIndex = 22;
            this.label11.Text = "Resident Status";
            // 
            // txtPostalAddress
            // 
            this.txtPostalAddress.Location = new System.Drawing.Point(184, 250);
            this.txtPostalAddress.Name = "txtPostalAddress";
            this.txtPostalAddress.Size = new System.Drawing.Size(542, 20);
            this.txtPostalAddress.TabIndex = 21;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(84, 253);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(77, 13);
            this.label1.TabIndex = 20;
            this.label1.Text = "Postal Address";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(20, 20);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(205, 29);
            this.label10.TabIndex = 1;
            this.label10.Text = "Personal Details";
            // 
            // txtCitizenship
            // 
            this.txtCitizenship.Location = new System.Drawing.Point(552, 213);
            this.txtCitizenship.Name = "txtCitizenship";
            this.txtCitizenship.Size = new System.Drawing.Size(172, 20);
            this.txtCitizenship.TabIndex = 19;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(473, 216);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(57, 13);
            this.label9.TabIndex = 18;
            this.label9.Text = "Citizenship";
            // 
            // txtNationalID
            // 
            this.txtNationalID.Location = new System.Drawing.Point(184, 213);
            this.txtNationalID.Name = "txtNationalID";
            this.txtNationalID.Size = new System.Drawing.Size(172, 20);
            this.txtNationalID.TabIndex = 17;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(81, 220);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(80, 13);
            this.label8.TabIndex = 16;
            this.label8.Text = "National ID No.";
            // 
            // txtPlaceOfBirth
            // 
            this.txtPlaceOfBirth.Location = new System.Drawing.Point(552, 177);
            this.txtPlaceOfBirth.Name = "txtPlaceOfBirth";
            this.txtPlaceOfBirth.Size = new System.Drawing.Size(172, 20);
            this.txtPlaceOfBirth.TabIndex = 15;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(460, 180);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(70, 13);
            this.label7.TabIndex = 14;
            this.label7.Text = "Place of Birth";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(505, 70);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(25, 13);
            this.label6.TabIndex = 4;
            this.label6.Text = "Sex";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(125, 180);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(36, 13);
            this.label5.TabIndex = 12;
            this.label5.Text = "D.O.B";
            // 
            // txtPreviousSurname
            // 
            this.txtPreviousSurname.Location = new System.Drawing.Point(184, 140);
            this.txtPreviousSurname.Name = "txtPreviousSurname";
            this.txtPreviousSurname.Size = new System.Drawing.Size(172, 20);
            this.txtPreviousSurname.TabIndex = 11;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(68, 143);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(93, 13);
            this.label4.TabIndex = 10;
            this.label4.Text = "Previous Surname";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(134, 65);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(27, 13);
            this.label3.TabIndex = 2;
            this.label3.Text = "Title";
            // 
            // txtSurname
            // 
            this.txtSurname.Location = new System.Drawing.Point(552, 102);
            this.txtSurname.Name = "txtSurname";
            this.txtSurname.Size = new System.Drawing.Size(172, 20);
            this.txtSurname.TabIndex = 9;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(93, 105);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(68, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "First Name(s)";
            // 
            // txtFirstName
            // 
            this.txtFirstName.Location = new System.Drawing.Point(184, 102);
            this.txtFirstName.Name = "txtFirstName";
            this.txtFirstName.Size = new System.Drawing.Size(234, 20);
            this.txtFirstName.TabIndex = 7;
            // 
            // lblSurname
            // 
            this.lblSurname.AutoSize = true;
            this.lblSurname.Location = new System.Drawing.Point(481, 102);
            this.lblSurname.Name = "lblSurname";
            this.lblSurname.Size = new System.Drawing.Size(49, 13);
            this.lblSurname.TabIndex = 8;
            this.lblSurname.Text = "&Surname";
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.txtKinEmail);
            this.tabPage2.Controls.Add(this.label27);
            this.tabPage2.Controls.Add(this.txtKinContacts);
            this.tabPage2.Controls.Add(this.label26);
            this.tabPage2.Controls.Add(this.txtKinPostalAddress);
            this.tabPage2.Controls.Add(this.label25);
            this.tabPage2.Controls.Add(this.txtKinSurname);
            this.tabPage2.Controls.Add(this.label24);
            this.tabPage2.Controls.Add(this.txtKinFirstName);
            this.tabPage2.Controls.Add(this.label23);
            this.tabPage2.Controls.Add(this.label17);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(754, 502);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Next of Kin/Guardian";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // txtKinEmail
            // 
            this.txtKinEmail.Location = new System.Drawing.Point(154, 224);
            this.txtKinEmail.Name = "txtKinEmail";
            this.txtKinEmail.Size = new System.Drawing.Size(490, 20);
            this.txtKinEmail.TabIndex = 44;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(93, 227);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(32, 13);
            this.label27.TabIndex = 43;
            this.label27.Text = "Email";
            // 
            // txtKinContacts
            // 
            this.txtKinContacts.Location = new System.Drawing.Point(154, 187);
            this.txtKinContacts.Name = "txtKinContacts";
            this.txtKinContacts.Size = new System.Drawing.Size(490, 20);
            this.txtKinContacts.TabIndex = 42;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(36, 187);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(89, 13);
            this.label26.TabIndex = 41;
            this.label26.Text = "Contact Numbers";
            // 
            // txtKinPostalAddress
            // 
            this.txtKinPostalAddress.Location = new System.Drawing.Point(154, 111);
            this.txtKinPostalAddress.Multiline = true;
            this.txtKinPostalAddress.Name = "txtKinPostalAddress";
            this.txtKinPostalAddress.Size = new System.Drawing.Size(490, 56);
            this.txtKinPostalAddress.TabIndex = 40;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(48, 114);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(77, 13);
            this.label25.TabIndex = 39;
            this.label25.Text = "Postal Address";
            // 
            // txtKinSurname
            // 
            this.txtKinSurname.Location = new System.Drawing.Point(461, 76);
            this.txtKinSurname.Name = "txtKinSurname";
            this.txtKinSurname.Size = new System.Drawing.Size(183, 20);
            this.txtKinSurname.TabIndex = 38;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(382, 79);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(49, 13);
            this.label24.TabIndex = 37;
            this.label24.Text = "Surname";
            // 
            // txtKinFirstName
            // 
            this.txtKinFirstName.Location = new System.Drawing.Point(154, 76);
            this.txtKinFirstName.Name = "txtKinFirstName";
            this.txtKinFirstName.Size = new System.Drawing.Size(187, 20);
            this.txtKinFirstName.TabIndex = 36;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(68, 79);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(57, 13);
            this.label23.TabIndex = 35;
            this.label23.Text = "First Name";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(20, 20);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(342, 29);
            this.label17.TabIndex = 34;
            this.label17.Text = "Next of Kin/Guardian Details";
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.btnClearExam);
            this.tabPage3.Controls.Add(this.btnExamRemove);
            this.tabPage3.Controls.Add(this.btmExamAdd);
            this.tabPage3.Controls.Add(this.txtExamResult);
            this.tabPage3.Controls.Add(this.label32);
            this.tabPage3.Controls.Add(this.txtExamSubject);
            this.tabPage3.Controls.Add(this.label31);
            this.tabPage3.Controls.Add(this.txtExamLevel);
            this.tabPage3.Controls.Add(this.label30);
            this.tabPage3.Controls.Add(this.txtExamBoard);
            this.tabPage3.Controls.Add(this.label29);
            this.tabPage3.Controls.Add(this.txtExamDate);
            this.tabPage3.Controls.Add(this.label28);
            this.tabPage3.Controls.Add(this.lvwExams);
            this.tabPage3.Controls.Add(this.label18);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(754, 502);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Exam Results";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // btnClearExam
            // 
            this.btnClearExam.Location = new System.Drawing.Point(639, 262);
            this.btnClearExam.Name = "btnClearExam";
            this.btnClearExam.Size = new System.Drawing.Size(75, 23);
            this.btnClearExam.TabIndex = 58;
            this.btnClearExam.Text = "Clear";
            this.btnClearExam.UseVisualStyleBackColor = true;
            this.btnClearExam.Click += new System.EventHandler(this.btnClearExam_Click);
            // 
            // btnExamRemove
            // 
            this.btnExamRemove.Location = new System.Drawing.Point(444, 429);
            this.btnExamRemove.Name = "btnExamRemove";
            this.btnExamRemove.Size = new System.Drawing.Size(75, 23);
            this.btnExamRemove.TabIndex = 59;
            this.btnExamRemove.Text = "Remove";
            this.btnExamRemove.UseVisualStyleBackColor = true;
            this.btnExamRemove.Click += new System.EventHandler(this.btnExamRemove_Click);
            // 
            // btmExamAdd
            // 
            this.btmExamAdd.Location = new System.Drawing.Point(541, 262);
            this.btmExamAdd.Name = "btmExamAdd";
            this.btmExamAdd.Size = new System.Drawing.Size(75, 23);
            this.btmExamAdd.TabIndex = 57;
            this.btmExamAdd.Text = "Add";
            this.btmExamAdd.UseVisualStyleBackColor = true;
            this.btmExamAdd.Click += new System.EventHandler(this.btmExamAdd_Click);
            // 
            // txtExamResult
            // 
            this.txtExamResult.Location = new System.Drawing.Point(541, 218);
            this.txtExamResult.Name = "txtExamResult";
            this.txtExamResult.Size = new System.Drawing.Size(73, 20);
            this.txtExamResult.TabIndex = 56;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(482, 221);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(37, 13);
            this.label32.TabIndex = 55;
            this.label32.Text = "Result";
            // 
            // txtExamSubject
            // 
            this.txtExamSubject.Location = new System.Drawing.Point(541, 182);
            this.txtExamSubject.Name = "txtExamSubject";
            this.txtExamSubject.Size = new System.Drawing.Size(173, 20);
            this.txtExamSubject.TabIndex = 54;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(476, 185);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(43, 13);
            this.label31.TabIndex = 53;
            this.label31.Text = "Subject";
            // 
            // txtExamLevel
            // 
            this.txtExamLevel.Location = new System.Drawing.Point(541, 145);
            this.txtExamLevel.Name = "txtExamLevel";
            this.txtExamLevel.Size = new System.Drawing.Size(173, 20);
            this.txtExamLevel.TabIndex = 52;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(486, 148);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(33, 13);
            this.label30.TabIndex = 51;
            this.label30.Text = "Level";
            // 
            // txtExamBoard
            // 
            this.txtExamBoard.Location = new System.Drawing.Point(541, 109);
            this.txtExamBoard.Name = "txtExamBoard";
            this.txtExamBoard.Size = new System.Drawing.Size(173, 20);
            this.txtExamBoard.TabIndex = 50;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(455, 112);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(64, 13);
            this.label29.TabIndex = 49;
            this.label29.Text = "Exam Board";
            // 
            // txtExamDate
            // 
            this.txtExamDate.Location = new System.Drawing.Point(541, 74);
            this.txtExamDate.Name = "txtExamDate";
            this.txtExamDate.Size = new System.Drawing.Size(100, 20);
            this.txtExamDate.TabIndex = 48;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(489, 77);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(30, 13);
            this.label28.TabIndex = 47;
            this.label28.Text = "Date";
            // 
            // lvwExams
            // 
            this.lvwExams.Location = new System.Drawing.Point(25, 69);
            this.lvwExams.Name = "lvwExams";
            this.lvwExams.Size = new System.Drawing.Size(413, 383);
            this.lvwExams.TabIndex = 46;
            this.lvwExams.UseCompatibleStateImageBehavior = false;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(20, 20);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(584, 29);
            this.label18.TabIndex = 45;
            this.label18.Text = "School Examination for Which Results are Known";
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.cmbLevel);
            this.tabPage4.Controls.Add(this.groupBox2);
            this.tabPage4.Controls.Add(this.groupBox3);
            this.tabPage4.Controls.Add(this.label37);
            this.tabPage4.Controls.Add(this.groupBox1);
            this.tabPage4.Controls.Add(this.label19);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Size = new System.Drawing.Size(754, 502);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "Programme";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // cmbLevel
            // 
            this.cmbLevel.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbLevel.FormattingEnabled = true;
            this.cmbLevel.Items.AddRange(new object[] {
            "Postgraduate",
            "Undergraduate",
            "Diploma",
            "Certificate"});
            this.cmbLevel.Location = new System.Drawing.Point(142, 70);
            this.cmbLevel.Name = "cmbLevel";
            this.cmbLevel.Size = new System.Drawing.Size(269, 21);
            this.cmbLevel.TabIndex = 61;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label38);
            this.groupBox2.Controls.Add(this.label39);
            this.groupBox2.Controls.Add(this.txtProgram2Option2);
            this.groupBox2.Controls.Add(this.txtProgram2Option1);
            this.groupBox2.Controls.Add(this.label40);
            this.groupBox2.Controls.Add(this.txtProgram2Name);
            this.groupBox2.Controls.Add(this.label41);
            this.groupBox2.Location = new System.Drawing.Point(25, 216);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(704, 101);
            this.groupBox2.TabIndex = 73;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Program 2";
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(407, 64);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(33, 13);
            this.label38.TabIndex = 79;
            this.label38.Text = "Opt 2";
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(119, 64);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(33, 13);
            this.label39.TabIndex = 77;
            this.label39.Text = "Opt 1";
            // 
            // txtProgram2Option2
            // 
            this.txtProgram2Option2.Location = new System.Drawing.Point(446, 61);
            this.txtProgram2Option2.Name = "txtProgram2Option2";
            this.txtProgram2Option2.Size = new System.Drawing.Size(230, 20);
            this.txtProgram2Option2.TabIndex = 80;
            // 
            // txtProgram2Option1
            // 
            this.txtProgram2Option1.Location = new System.Drawing.Point(158, 61);
            this.txtProgram2Option1.Name = "txtProgram2Option1";
            this.txtProgram2Option1.Size = new System.Drawing.Size(228, 20);
            this.txtProgram2Option1.TabIndex = 78;
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Location = new System.Drawing.Point(20, 64);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(72, 13);
            this.label40.TabIndex = 76;
            this.label40.Text = "Specialization";
            // 
            // txtProgram2Name
            // 
            this.txtProgram2Name.Location = new System.Drawing.Point(117, 29);
            this.txtProgram2Name.Name = "txtProgram2Name";
            this.txtProgram2Name.Size = new System.Drawing.Size(559, 20);
            this.txtProgram2Name.TabIndex = 75;
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Location = new System.Drawing.Point(20, 29);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(91, 13);
            this.label41.TabIndex = 74;
            this.label41.Text = "Programme Name";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.label42);
            this.groupBox3.Controls.Add(this.label43);
            this.groupBox3.Controls.Add(this.txtProgram3Option2);
            this.groupBox3.Controls.Add(this.txtProgram3Option1);
            this.groupBox3.Controls.Add(this.label44);
            this.groupBox3.Controls.Add(this.txtProgram3Name);
            this.groupBox3.Controls.Add(this.label45);
            this.groupBox3.Location = new System.Drawing.Point(25, 323);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(704, 101);
            this.groupBox3.TabIndex = 81;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Program 3";
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Location = new System.Drawing.Point(407, 64);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(33, 13);
            this.label42.TabIndex = 87;
            this.label42.Text = "Opt 2";
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Location = new System.Drawing.Point(119, 64);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(33, 13);
            this.label43.TabIndex = 85;
            this.label43.Text = "Opt 1";
            // 
            // txtProgram3Option2
            // 
            this.txtProgram3Option2.Location = new System.Drawing.Point(446, 61);
            this.txtProgram3Option2.Name = "txtProgram3Option2";
            this.txtProgram3Option2.Size = new System.Drawing.Size(230, 20);
            this.txtProgram3Option2.TabIndex = 88;
            // 
            // txtProgram3Option1
            // 
            this.txtProgram3Option1.Location = new System.Drawing.Point(158, 61);
            this.txtProgram3Option1.Name = "txtProgram3Option1";
            this.txtProgram3Option1.Size = new System.Drawing.Size(228, 20);
            this.txtProgram3Option1.TabIndex = 86;
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Location = new System.Drawing.Point(20, 64);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(72, 13);
            this.label44.TabIndex = 84;
            this.label44.Text = "Specialization";
            // 
            // txtProgram3Name
            // 
            this.txtProgram3Name.Location = new System.Drawing.Point(117, 29);
            this.txtProgram3Name.Name = "txtProgram3Name";
            this.txtProgram3Name.Size = new System.Drawing.Size(559, 20);
            this.txtProgram3Name.TabIndex = 83;
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Location = new System.Drawing.Point(20, 29);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(91, 13);
            this.label45.TabIndex = 82;
            this.label45.Text = "Programme Name";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(45, 70);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(33, 13);
            this.label37.TabIndex = 60;
            this.label37.Text = "Level";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label36);
            this.groupBox1.Controls.Add(this.label35);
            this.groupBox1.Controls.Add(this.txtProgram1Option2);
            this.groupBox1.Controls.Add(this.txtProgram1Option1);
            this.groupBox1.Controls.Add(this.label34);
            this.groupBox1.Controls.Add(this.txtProgram1Name);
            this.groupBox1.Controls.Add(this.label33);
            this.groupBox1.Location = new System.Drawing.Point(25, 109);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(704, 101);
            this.groupBox1.TabIndex = 65;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Program 1";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(407, 64);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(33, 13);
            this.label36.TabIndex = 71;
            this.label36.Text = "Opt 2";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(119, 64);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(33, 13);
            this.label35.TabIndex = 69;
            this.label35.Text = "Opt 1";
            // 
            // txtProgram1Option2
            // 
            this.txtProgram1Option2.Location = new System.Drawing.Point(446, 61);
            this.txtProgram1Option2.Name = "txtProgram1Option2";
            this.txtProgram1Option2.Size = new System.Drawing.Size(230, 20);
            this.txtProgram1Option2.TabIndex = 72;
            // 
            // txtProgram1Option1
            // 
            this.txtProgram1Option1.Location = new System.Drawing.Point(158, 61);
            this.txtProgram1Option1.Name = "txtProgram1Option1";
            this.txtProgram1Option1.Size = new System.Drawing.Size(228, 20);
            this.txtProgram1Option1.TabIndex = 70;
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(20, 64);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(72, 13);
            this.label34.TabIndex = 68;
            this.label34.Text = "Specialization";
            // 
            // txtProgram1Name
            // 
            this.txtProgram1Name.Location = new System.Drawing.Point(117, 29);
            this.txtProgram1Name.Name = "txtProgram1Name";
            this.txtProgram1Name.Size = new System.Drawing.Size(559, 20);
            this.txtProgram1Name.TabIndex = 67;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(20, 29);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(91, 13);
            this.label33.TabIndex = 66;
            this.label33.Text = "Programme Name";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(20, 20);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(399, 29);
            this.label19.TabIndex = 59;
            this.label19.Text = "Programme(s) Being Applied For";
            // 
            // tabPage5
            // 
            this.tabPage5.Controls.Add(this.txtReference2);
            this.tabPage5.Controls.Add(this.label53);
            this.tabPage5.Controls.Add(this.txtReference1);
            this.tabPage5.Controls.Add(this.label52);
            this.tabPage5.Controls.Add(this.txtNoticePeriod);
            this.tabPage5.Controls.Add(this.label51);
            this.tabPage5.Controls.Add(this.groupBox4);
            this.tabPage5.Controls.Add(this.txtCurrentEmployer);
            this.tabPage5.Controls.Add(this.label46);
            this.tabPage5.Controls.Add(this.dtpEmpTo);
            this.tabPage5.Controls.Add(this.label49);
            this.tabPage5.Controls.Add(this.dtpEmpFrom);
            this.tabPage5.Controls.Add(this.btnEmpRemove);
            this.tabPage5.Controls.Add(this.btnEmpAdd);
            this.tabPage5.Controls.Add(this.txtDuties);
            this.tabPage5.Controls.Add(this.label47);
            this.tabPage5.Controls.Add(this.txtEmployer);
            this.tabPage5.Controls.Add(this.label48);
            this.tabPage5.Controls.Add(this.label50);
            this.tabPage5.Controls.Add(this.lvwEmployment);
            this.tabPage5.Controls.Add(this.label20);
            this.tabPage5.Location = new System.Drawing.Point(4, 22);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Size = new System.Drawing.Size(754, 502);
            this.tabPage5.TabIndex = 4;
            this.tabPage5.Text = "Employment";
            this.tabPage5.UseVisualStyleBackColor = true;
            // 
            // txtReference2
            // 
            this.txtReference2.Location = new System.Drawing.Point(454, 391);
            this.txtReference2.Multiline = true;
            this.txtReference2.Name = "txtReference2";
            this.txtReference2.Size = new System.Drawing.Size(268, 66);
            this.txtReference2.TabIndex = 109;
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.Location = new System.Drawing.Point(395, 394);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(33, 13);
            this.label53.TabIndex = 108;
            this.label53.Text = "Ref 2";
            // 
            // txtReference1
            // 
            this.txtReference1.Location = new System.Drawing.Point(86, 388);
            this.txtReference1.Multiline = true;
            this.txtReference1.Name = "txtReference1";
            this.txtReference1.Size = new System.Drawing.Size(268, 66);
            this.txtReference1.TabIndex = 107;
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.Location = new System.Drawing.Point(22, 391);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(33, 13);
            this.label52.TabIndex = 106;
            this.label52.Text = "Ref 1";
            // 
            // txtNoticePeriod
            // 
            this.txtNoticePeriod.Location = new System.Drawing.Point(609, 351);
            this.txtNoticePeriod.Name = "txtNoticePeriod";
            this.txtNoticePeriod.Size = new System.Drawing.Size(116, 20);
            this.txtNoticePeriod.TabIndex = 105;
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Location = new System.Drawing.Point(532, 354);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(71, 13);
            this.label51.TabIndex = 104;
            this.label51.Text = "Notice Period";
            // 
            // groupBox4
            // 
            this.groupBox4.Location = new System.Drawing.Point(9, 329);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(742, 5);
            this.groupBox4.TabIndex = 101;
            this.groupBox4.TabStop = false;
            // 
            // txtCurrentEmployer
            // 
            this.txtCurrentEmployer.Location = new System.Drawing.Point(137, 351);
            this.txtCurrentEmployer.Name = "txtCurrentEmployer";
            this.txtCurrentEmployer.Size = new System.Drawing.Size(370, 20);
            this.txtCurrentEmployer.TabIndex = 103;
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Location = new System.Drawing.Point(22, 351);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(87, 13);
            this.label46.TabIndex = 102;
            this.label46.Text = "Current Employer";
            // 
            // dtpEmpTo
            // 
            this.dtpEmpTo.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpEmpTo.Location = new System.Drawing.Point(642, 62);
            this.dtpEmpTo.Name = "dtpEmpTo";
            this.dtpEmpTo.Size = new System.Drawing.Size(81, 20);
            this.dtpEmpTo.TabIndex = 95;
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Location = new System.Drawing.Point(606, 67);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(20, 13);
            this.label49.TabIndex = 94;
            this.label49.Text = "To";
            // 
            // dtpEmpFrom
            // 
            this.dtpEmpFrom.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpEmpFrom.Location = new System.Drawing.Point(494, 62);
            this.dtpEmpFrom.Name = "dtpEmpFrom";
            this.dtpEmpFrom.Size = new System.Drawing.Size(81, 20);
            this.dtpEmpFrom.TabIndex = 93;
            // 
            // btnEmpRemove
            // 
            this.btnEmpRemove.Location = new System.Drawing.Point(353, 300);
            this.btnEmpRemove.Name = "btnEmpRemove";
            this.btnEmpRemove.Size = new System.Drawing.Size(75, 23);
            this.btnEmpRemove.TabIndex = 91;
            this.btnEmpRemove.Text = "Remove";
            this.btnEmpRemove.UseVisualStyleBackColor = true;
            this.btnEmpRemove.Click += new System.EventHandler(this.btnEmpRemove_Click);
            // 
            // btnEmpAdd
            // 
            this.btnEmpAdd.Location = new System.Drawing.Point(642, 271);
            this.btnEmpAdd.Name = "btnEmpAdd";
            this.btnEmpAdd.Size = new System.Drawing.Size(75, 23);
            this.btnEmpAdd.TabIndex = 100;
            this.btnEmpAdd.Text = "Add";
            this.btnEmpAdd.UseVisualStyleBackColor = true;
            this.btnEmpAdd.Click += new System.EventHandler(this.btnEmpAdd_Click);
            // 
            // txtDuties
            // 
            this.txtDuties.Location = new System.Drawing.Point(449, 192);
            this.txtDuties.Multiline = true;
            this.txtDuties.Name = "txtDuties";
            this.txtDuties.Size = new System.Drawing.Size(274, 69);
            this.txtDuties.TabIndex = 99;
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Location = new System.Drawing.Point(446, 167);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(37, 13);
            this.label47.TabIndex = 98;
            this.label47.Text = "Duties";
            // 
            // txtEmployer
            // 
            this.txtEmployer.Location = new System.Drawing.Point(449, 131);
            this.txtEmployer.Name = "txtEmployer";
            this.txtEmployer.Size = new System.Drawing.Size(274, 20);
            this.txtEmployer.TabIndex = 97;
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Location = new System.Drawing.Point(446, 104);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(110, 13);
            this.label48.TabIndex = 96;
            this.label48.Text = "Employer/Occupation";
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Location = new System.Drawing.Point(446, 67);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(30, 13);
            this.label50.TabIndex = 92;
            this.label50.Text = "From";
            // 
            // lvwEmployment
            // 
            this.lvwEmployment.Location = new System.Drawing.Point(25, 62);
            this.lvwEmployment.Name = "lvwEmployment";
            this.lvwEmployment.Size = new System.Drawing.Size(403, 232);
            this.lvwEmployment.TabIndex = 90;
            this.lvwEmployment.UseCompatibleStateImageBehavior = false;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(20, 20);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(343, 29);
            this.label20.TabIndex = 89;
            this.label20.Text = "Further Relevant Information";
            // 
            // tabPage6
            // 
            this.tabPage6.Controls.Add(this.txtEmployeeNo);
            this.tabPage6.Controls.Add(this.label59);
            this.tabPage6.Controls.Add(this.txtStaffDepartment);
            this.tabPage6.Controls.Add(this.label58);
            this.tabPage6.Controls.Add(this.cmbIsStaff);
            this.tabPage6.Controls.Add(this.label56);
            this.tabPage6.Controls.Add(this.cmbIsStaffDependant);
            this.tabPage6.Controls.Add(this.label55);
            this.tabPage6.Controls.Add(this.txtSponsorName);
            this.tabPage6.Controls.Add(this.label54);
            this.tabPage6.Controls.Add(this.label21);
            this.tabPage6.Location = new System.Drawing.Point(4, 22);
            this.tabPage6.Name = "tabPage6";
            this.tabPage6.Size = new System.Drawing.Size(754, 502);
            this.tabPage6.TabIndex = 5;
            this.tabPage6.Text = "Sponsor";
            this.tabPage6.UseVisualStyleBackColor = true;
            // 
            // txtEmployeeNo
            // 
            this.txtEmployeeNo.Location = new System.Drawing.Point(464, 194);
            this.txtEmployeeNo.Name = "txtEmployeeNo";
            this.txtEmployeeNo.Size = new System.Drawing.Size(175, 20);
            this.txtEmployeeNo.TabIndex = 122;
            // 
            // label59
            // 
            this.label59.AutoSize = true;
            this.label59.Location = new System.Drawing.Point(346, 197);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(93, 13);
            this.label59.TabIndex = 121;
            this.label59.Text = "Employee Number";
            // 
            // txtStaffDepartment
            // 
            this.txtStaffDepartment.Location = new System.Drawing.Point(464, 158);
            this.txtStaffDepartment.Name = "txtStaffDepartment";
            this.txtStaffDepartment.Size = new System.Drawing.Size(262, 20);
            this.txtStaffDepartment.TabIndex = 120;
            // 
            // label58
            // 
            this.label58.AutoSize = true;
            this.label58.Location = new System.Drawing.Point(377, 161);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(62, 13);
            this.label58.TabIndex = 119;
            this.label58.Text = "Department";
            // 
            // cmbIsStaff
            // 
            this.cmbIsStaff.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbIsStaff.FormattingEnabled = true;
            this.cmbIsStaff.Items.AddRange(new object[] {
            "Yes",
            "No"});
            this.cmbIsStaff.Location = new System.Drawing.Point(191, 157);
            this.cmbIsStaff.Name = "cmbIsStaff";
            this.cmbIsStaff.Size = new System.Drawing.Size(121, 21);
            this.cmbIsStaff.TabIndex = 116;
            // 
            // label56
            // 
            this.label56.AutoSize = true;
            this.label56.Location = new System.Drawing.Point(87, 157);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(76, 13);
            this.label56.TabIndex = 115;
            this.label56.Text = "Staff Member?";
            // 
            // cmbIsStaffDependant
            // 
            this.cmbIsStaffDependant.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbIsStaffDependant.FormattingEnabled = true;
            this.cmbIsStaffDependant.Items.AddRange(new object[] {
            "Yes",
            "No"});
            this.cmbIsStaffDependant.Location = new System.Drawing.Point(191, 117);
            this.cmbIsStaffDependant.Name = "cmbIsStaffDependant";
            this.cmbIsStaffDependant.Size = new System.Drawing.Size(121, 21);
            this.cmbIsStaffDependant.TabIndex = 114;
            // 
            // label55
            // 
            this.label55.AutoSize = true;
            this.label55.Location = new System.Drawing.Point(25, 117);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(138, 13);
            this.label55.TabIndex = 113;
            this.label55.Text = "University staff Dependant?";
            // 
            // txtSponsorName
            // 
            this.txtSponsorName.Location = new System.Drawing.Point(191, 78);
            this.txtSponsorName.Name = "txtSponsorName";
            this.txtSponsorName.Size = new System.Drawing.Size(535, 20);
            this.txtSponsorName.TabIndex = 112;
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.Location = new System.Drawing.Point(88, 81);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(77, 13);
            this.label54.TabIndex = 111;
            this.label54.Text = "Sponsor Name";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(20, 20);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(287, 29);
            this.label21.TabIndex = 110;
            this.label21.Text = "Prospective Sponsor(s)";
            // 
            // tabPage7
            // 
            this.tabPage7.Controls.Add(this.cmbHasBirthCertificate);
            this.tabPage7.Controls.Add(this.cmbHasTranscript);
            this.tabPage7.Controls.Add(this.cmbHasCertificate);
            this.tabPage7.Controls.Add(this.cmbHasDiplomaOrDegree);
            this.tabPage7.Controls.Add(this.cmbHasALevel);
            this.tabPage7.Controls.Add(this.cmbHasMaths);
            this.tabPage7.Controls.Add(this.cmbHasEnglish);
            this.tabPage7.Controls.Add(this.cmbHasFiveOLevels);
            this.tabPage7.Controls.Add(this.label69);
            this.tabPage7.Controls.Add(this.label68);
            this.tabPage7.Controls.Add(this.txtOtherCertificates);
            this.tabPage7.Controls.Add(this.label67);
            this.tabPage7.Controls.Add(this.label66);
            this.tabPage7.Controls.Add(this.label65);
            this.tabPage7.Controls.Add(this.label64);
            this.tabPage7.Controls.Add(this.label63);
            this.tabPage7.Controls.Add(this.label62);
            this.tabPage7.Controls.Add(this.label61);
            this.tabPage7.Controls.Add(this.cmbHasIdentification);
            this.tabPage7.Controls.Add(this.label60);
            this.tabPage7.Controls.Add(this.label22);
            this.tabPage7.Location = new System.Drawing.Point(4, 22);
            this.tabPage7.Name = "tabPage7";
            this.tabPage7.Size = new System.Drawing.Size(754, 502);
            this.tabPage7.TabIndex = 6;
            this.tabPage7.Text = "Checklist";
            this.tabPage7.UseVisualStyleBackColor = true;
            // 
            // cmbHasBirthCertificate
            // 
            this.cmbHasBirthCertificate.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbHasBirthCertificate.FormattingEnabled = true;
            this.cmbHasBirthCertificate.Items.AddRange(new object[] {
            "Yes",
            "No"});
            this.cmbHasBirthCertificate.Location = new System.Drawing.Point(171, 302);
            this.cmbHasBirthCertificate.Name = "cmbHasBirthCertificate";
            this.cmbHasBirthCertificate.Size = new System.Drawing.Size(87, 21);
            this.cmbHasBirthCertificate.TabIndex = 141;
            // 
            // cmbHasTranscript
            // 
            this.cmbHasTranscript.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbHasTranscript.FormattingEnabled = true;
            this.cmbHasTranscript.Items.AddRange(new object[] {
            "Yes",
            "No"});
            this.cmbHasTranscript.Location = new System.Drawing.Point(170, 264);
            this.cmbHasTranscript.Name = "cmbHasTranscript";
            this.cmbHasTranscript.Size = new System.Drawing.Size(87, 21);
            this.cmbHasTranscript.TabIndex = 139;
            // 
            // cmbHasCertificate
            // 
            this.cmbHasCertificate.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbHasCertificate.FormattingEnabled = true;
            this.cmbHasCertificate.Items.AddRange(new object[] {
            "Yes",
            "No"});
            this.cmbHasCertificate.Location = new System.Drawing.Point(170, 230);
            this.cmbHasCertificate.Name = "cmbHasCertificate";
            this.cmbHasCertificate.Size = new System.Drawing.Size(87, 21);
            this.cmbHasCertificate.TabIndex = 137;
            // 
            // cmbHasDiplomaOrDegree
            // 
            this.cmbHasDiplomaOrDegree.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbHasDiplomaOrDegree.FormattingEnabled = true;
            this.cmbHasDiplomaOrDegree.Items.AddRange(new object[] {
            "Yes",
            "No"});
            this.cmbHasDiplomaOrDegree.Location = new System.Drawing.Point(171, 188);
            this.cmbHasDiplomaOrDegree.Name = "cmbHasDiplomaOrDegree";
            this.cmbHasDiplomaOrDegree.Size = new System.Drawing.Size(87, 21);
            this.cmbHasDiplomaOrDegree.TabIndex = 135;
            // 
            // cmbHasALevel
            // 
            this.cmbHasALevel.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbHasALevel.FormattingEnabled = true;
            this.cmbHasALevel.Items.AddRange(new object[] {
            "Yes",
            "No"});
            this.cmbHasALevel.Location = new System.Drawing.Point(170, 149);
            this.cmbHasALevel.Name = "cmbHasALevel";
            this.cmbHasALevel.Size = new System.Drawing.Size(87, 21);
            this.cmbHasALevel.TabIndex = 133;
            // 
            // cmbHasMaths
            // 
            this.cmbHasMaths.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbHasMaths.FormattingEnabled = true;
            this.cmbHasMaths.Items.AddRange(new object[] {
            "Yes",
            "No"});
            this.cmbHasMaths.Location = new System.Drawing.Point(600, 111);
            this.cmbHasMaths.Name = "cmbHasMaths";
            this.cmbHasMaths.Size = new System.Drawing.Size(87, 21);
            this.cmbHasMaths.TabIndex = 131;
            // 
            // cmbHasEnglish
            // 
            this.cmbHasEnglish.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbHasEnglish.FormattingEnabled = true;
            this.cmbHasEnglish.Items.AddRange(new object[] {
            "Yes",
            "No"});
            this.cmbHasEnglish.Location = new System.Drawing.Point(386, 109);
            this.cmbHasEnglish.Name = "cmbHasEnglish";
            this.cmbHasEnglish.Size = new System.Drawing.Size(87, 21);
            this.cmbHasEnglish.TabIndex = 129;
            this.cmbHasEnglish.SelectedIndexChanged += new System.EventHandler(this.comboBox10_SelectedIndexChanged);
            // 
            // cmbHasFiveOLevels
            // 
            this.cmbHasFiveOLevels.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbHasFiveOLevels.FormattingEnabled = true;
            this.cmbHasFiveOLevels.Items.AddRange(new object[] {
            "Yes",
            "No"});
            this.cmbHasFiveOLevels.Location = new System.Drawing.Point(170, 109);
            this.cmbHasFiveOLevels.Name = "cmbHasFiveOLevels";
            this.cmbHasFiveOLevels.Size = new System.Drawing.Size(87, 21);
            this.cmbHasFiveOLevels.TabIndex = 127;
            // 
            // label69
            // 
            this.label69.AutoSize = true;
            this.label69.Location = new System.Drawing.Point(531, 114);
            this.label69.Name = "label69";
            this.label69.Size = new System.Drawing.Size(36, 13);
            this.label69.TabIndex = 130;
            this.label69.Text = "Maths";
            // 
            // label68
            // 
            this.label68.AutoSize = true;
            this.label68.Location = new System.Drawing.Point(315, 114);
            this.label68.Name = "label68";
            this.label68.Size = new System.Drawing.Size(41, 13);
            this.label68.TabIndex = 128;
            this.label68.Text = "English";
            // 
            // txtOtherCertificates
            // 
            this.txtOtherCertificates.Location = new System.Drawing.Point(171, 343);
            this.txtOtherCertificates.Multiline = true;
            this.txtOtherCertificates.Name = "txtOtherCertificates";
            this.txtOtherCertificates.Size = new System.Drawing.Size(516, 80);
            this.txtOtherCertificates.TabIndex = 143;
            // 
            // label67
            // 
            this.label67.AutoSize = true;
            this.label67.Location = new System.Drawing.Point(51, 346);
            this.label67.Name = "label67";
            this.label67.Size = new System.Drawing.Size(88, 13);
            this.label67.TabIndex = 142;
            this.label67.Text = "Other Certificates";
            // 
            // label66
            // 
            this.label66.AutoSize = true;
            this.label66.Location = new System.Drawing.Point(62, 310);
            this.label66.Name = "label66";
            this.label66.Size = new System.Drawing.Size(78, 13);
            this.label66.TabIndex = 140;
            this.label66.Text = "Birth Certificate";
            // 
            // label65
            // 
            this.label65.AutoSize = true;
            this.label65.Location = new System.Drawing.Point(86, 267);
            this.label65.Name = "label65";
            this.label65.Size = new System.Drawing.Size(54, 13);
            this.label65.TabIndex = 138;
            this.label65.Text = "Transcript";
            // 
            // label64
            // 
            this.label64.AutoSize = true;
            this.label64.Location = new System.Drawing.Point(85, 233);
            this.label64.Name = "label64";
            this.label64.Size = new System.Drawing.Size(54, 13);
            this.label64.TabIndex = 136;
            this.label64.Text = "Certificate";
            // 
            // label63
            // 
            this.label63.AutoSize = true;
            this.label63.Location = new System.Drawing.Point(55, 191);
            this.label63.Name = "label63";
            this.label63.Size = new System.Drawing.Size(85, 13);
            this.label63.TabIndex = 134;
            this.label63.Text = "Degree/Diploma";
            // 
            // label62
            // 
            this.label62.AutoSize = true;
            this.label62.Location = new System.Drawing.Point(97, 152);
            this.label62.Name = "label62";
            this.label62.Size = new System.Drawing.Size(42, 13);
            this.label62.TabIndex = 132;
            this.label62.Text = "A\'Level";
            // 
            // label61
            // 
            this.label61.AutoSize = true;
            this.label61.Location = new System.Drawing.Point(82, 114);
            this.label61.Name = "label61";
            this.label61.Size = new System.Drawing.Size(57, 13);
            this.label61.TabIndex = 126;
            this.label61.Text = "5 O\'Levels";
            // 
            // cmbHasIdentification
            // 
            this.cmbHasIdentification.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbHasIdentification.FormattingEnabled = true;
            this.cmbHasIdentification.Items.AddRange(new object[] {
            "Yes",
            "No"});
            this.cmbHasIdentification.Location = new System.Drawing.Point(170, 71);
            this.cmbHasIdentification.Name = "cmbHasIdentification";
            this.cmbHasIdentification.Size = new System.Drawing.Size(87, 21);
            this.cmbHasIdentification.TabIndex = 125;
            // 
            // label60
            // 
            this.label60.AutoSize = true;
            this.label60.Location = new System.Drawing.Point(33, 74);
            this.label60.Name = "label60";
            this.label60.Size = new System.Drawing.Size(106, 13);
            this.label60.TabIndex = 124;
            this.label60.Text = "National ID/Passport";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(20, 20);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(120, 29);
            this.label22.TabIndex = 123;
            this.label22.Text = "Checklist";
            // 
            // btnSave
            // 
            this.btnSave.Location = new System.Drawing.Point(616, 560);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(75, 33);
            this.btnSave.TabIndex = 144;
            this.btnSave.Text = "&Save";
            this.btnSave.UseVisualStyleBackColor = true;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(707, 560);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 33);
            this.btnCancel.TabIndex = 145;
            this.btnCancel.Text = "&Cancel";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // btnClear
            // 
            this.btnClear.Location = new System.Drawing.Point(20, 558);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(75, 33);
            this.btnClear.TabIndex = 146;
            this.btnClear.Text = "Clear";
            this.btnClear.UseVisualStyleBackColor = true;
            // 
            // txtEmail
            // 
            this.txtEmail.Location = new System.Drawing.Point(185, 286);
            this.txtEmail.Name = "txtEmail";
            this.txtEmail.Size = new System.Drawing.Size(292, 20);
            this.txtEmail.TabIndex = 21;
            // 
            // label71
            // 
            this.label71.AutoSize = true;
            this.label71.Location = new System.Drawing.Point(492, 289);
            this.label71.Name = "label71";
            this.label71.Size = new System.Drawing.Size(38, 13);
            this.label71.TabIndex = 34;
            this.label71.Text = "Phone";
            // 
            // txtPhone
            // 
            this.txtPhone.Location = new System.Drawing.Point(552, 286);
            this.txtPhone.Name = "txtPhone";
            this.txtPhone.Size = new System.Drawing.Size(172, 20);
            this.txtPhone.TabIndex = 21;
            // 
            // label72
            // 
            this.label72.AutoSize = true;
            this.label72.Location = new System.Drawing.Point(129, 289);
            this.label72.Name = "label72";
            this.label72.Size = new System.Drawing.Size(32, 13);
            this.label72.TabIndex = 36;
            this.label72.Text = "Email";
            // 
            // txtId
            // 
            this.txtId.Location = new System.Drawing.Point(707, 2);
            this.txtId.Name = "txtId";
            this.txtId.Size = new System.Drawing.Size(100, 20);
            this.txtId.TabIndex = 147;
            this.txtId.TabStop = false;
            this.txtId.Visible = false;
            // 
            // frmRegister
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(806, 605);
            this.Controls.Add(this.txtId);
            this.Controls.Add(this.btnClear);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.tabPersonalDetails);
            this.Name = "frmRegister";
            this.Text = "Student Registration";
            this.Load += new System.EventHandler(this.frmStudentRegistration_Load);
            this.tabPersonalDetails.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            this.tabPage4.ResumeLayout(false);
            this.tabPage4.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.tabPage5.ResumeLayout(false);
            this.tabPage5.PerformLayout();
            this.tabPage6.ResumeLayout(false);
            this.tabPage6.PerformLayout();
            this.tabPage7.ResumeLayout(false);
            this.tabPage7.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TabControl tabPersonalDetails;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TextBox txtDisabilities;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox txtPassportCountry;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox txtPassportNo;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txtResidencePeriod;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txtPermit;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txtPostalAddress;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtCitizenship;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtNationalID;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtPlaceOfBirth;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtPreviousSurname;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtSurname;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtFirstName;
        private System.Windows.Forms.Label lblSurname;
        private System.Windows.Forms.DateTimePicker dtpDOB;
        private System.Windows.Forms.ComboBox cmbSex;
        private System.Windows.Forms.ComboBox cmbTitle;
        private System.Windows.Forms.TextBox txtKinEmail;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.TextBox txtKinContacts;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.TextBox txtKinPostalAddress;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.TextBox txtKinSurname;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.TextBox txtKinFirstName;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.Button btnExamRemove;
        private System.Windows.Forms.Button btmExamAdd;
        private System.Windows.Forms.TextBox txtExamResult;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.TextBox txtExamSubject;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.TextBox txtExamLevel;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.TextBox txtExamBoard;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.TextBox txtExamDate;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.ListView lvwExams;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.TextBox txtProgram2Option2;
        private System.Windows.Forms.TextBox txtProgram2Option1;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.TextBox txtProgram2Name;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.TextBox txtProgram3Option2;
        private System.Windows.Forms.TextBox txtProgram3Option1;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.TextBox txtProgram3Name;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.TextBox txtProgram1Option2;
        private System.Windows.Forms.TextBox txtProgram1Option1;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.TextBox txtProgram1Name;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.TextBox txtReference2;
        private System.Windows.Forms.Label label53;
        private System.Windows.Forms.TextBox txtReference1;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.TextBox txtNoticePeriod;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.TextBox txtCurrentEmployer;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.DateTimePicker dtpEmpTo;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.DateTimePicker dtpEmpFrom;
        private System.Windows.Forms.Button btnEmpRemove;
        private System.Windows.Forms.Button btnEmpAdd;
        private System.Windows.Forms.TextBox txtDuties;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.TextBox txtEmployer;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.ListView lvwEmployment;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TabPage tabPage6;
        private System.Windows.Forms.TextBox txtEmployeeNo;
        private System.Windows.Forms.Label label59;
        private System.Windows.Forms.TextBox txtStaffDepartment;
        private System.Windows.Forms.Label label58;
        private System.Windows.Forms.ComboBox cmbIsStaff;
        private System.Windows.Forms.Label label56;
        private System.Windows.Forms.ComboBox cmbIsStaffDependant;
        private System.Windows.Forms.Label label55;
        private System.Windows.Forms.TextBox txtSponsorName;
        private System.Windows.Forms.Label label54;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TabPage tabPage7;
        private System.Windows.Forms.ComboBox cmbHasBirthCertificate;
        private System.Windows.Forms.ComboBox cmbHasTranscript;
        private System.Windows.Forms.ComboBox cmbHasCertificate;
        private System.Windows.Forms.ComboBox cmbHasDiplomaOrDegree;
        private System.Windows.Forms.ComboBox cmbHasALevel;
        private System.Windows.Forms.ComboBox cmbHasMaths;
        private System.Windows.Forms.ComboBox cmbHasEnglish;
        private System.Windows.Forms.ComboBox cmbHasFiveOLevels;
        private System.Windows.Forms.Label label69;
        private System.Windows.Forms.Label label68;
        private System.Windows.Forms.TextBox txtOtherCertificates;
        private System.Windows.Forms.Label label67;
        private System.Windows.Forms.Label label66;
        private System.Windows.Forms.Label label65;
        private System.Windows.Forms.Label label64;
        private System.Windows.Forms.Label label63;
        private System.Windows.Forms.Label label62;
        private System.Windows.Forms.Label label61;
        private System.Windows.Forms.ComboBox cmbHasIdentification;
        private System.Windows.Forms.Label label60;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.Label label70;
        private System.Windows.Forms.ComboBox cmbResidentStatus;
        private System.Windows.Forms.ComboBox cmbConsiderForResidence;
        private System.Windows.Forms.ComboBox cmbLevel;
        private System.Windows.Forms.Button btnClearExam;
        private System.Windows.Forms.TextBox txtPhone;
        private System.Windows.Forms.Label label72;
        private System.Windows.Forms.TextBox txtEmail;
        private System.Windows.Forms.Label label71;
        private System.Windows.Forms.TextBox txtId;
    }
}

