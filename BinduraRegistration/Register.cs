﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlServerCe;

namespace BinduraRegistration
{
    class Register
    {

    	public int id; 
        public String regno; 
        public string title; 
        public string firstname; 
        public string surname; 
        public string previous_surname; 
        public string dob; 
        public string place_of_birth; 
        public string national_id; 
        public string citizenship; 
        public string postal_address;
        public string isResident;
        public string permit;
        public string period_of_residence;
        public string passport_no;

        public string passport_place_of_issue; 
        public string needs_residence; 
        public string disabilities; 
        public string sex; 
        public string phone; 
        public string email; 
        public string next_of_kin_firstname;

        public string next_of_kin_surname; 
        public string next_of_kin_postal_address; 
        public string next_of_kin_phone; 

        public string next_of_kin_email; 
        public string emp_current_employer; 
        public string emp_notice_period; 

        public string emp_reference_1; 
        public string emp_reference_2; 
        public string sponsor_name; 

        public String sponsor_isStaffDependant; 
        public string sponsor_isStaff; 
        public string sponsor_staff_department; 
        public string sponsor_staff_employee_number; 
        public string checklist_identification; 
        public string checklist_5_O_Levels; 
        public string checklist_English; 
        public string checklist_Mathematics; 
        public string checklist_A_Level;
        public string checklist_degree_diploma;
        public string checklist_certificate;

        public String checklist_transcript;
        public string checklist_birth_certificate; 

        public string checklist_other_certificates;
        public string programme_level;
        public string programme_1_name; 
        public string programme_1_option_1; 
        public string programme_1_option_2; 
        public string programme_2_name; 
        public string programme_2_option_1; 
        public string programme_2_option_2; 
        public string programme_3_name;
        public string programme_3_option_1; 
        public string programme_3_option_2; 

        public Register(int id, string regno, string title, string firstname, string surname, string previous_surname, string dob, 
            string place_of_birth, string national_id, string citizenship, string postal_address, string isResident, string permit, 
            string period_of_residence, string passport_no, string passport_place_of_issue, string needs_residence, string disabilities, 
            string sex, string phone, string email, string next_of_kin_firstname, string next_of_kin_surname, string next_of_kin_postal_address, 
            string next_of_kin_phone, string next_of_kin_email, string emp_current_employer, string emp_notice_period, string emp_reference_1, 
            string emp_reference_2, string sponsor_name, string sponsor_isStaffDependant, string sponsor_isStaff, string sponsor_staff_department, 
            string sponsor_staff_employee_number, string checklist_identification, string checklist_5_O_Levels, string checklist_English, 
            string checklist_Mathematics, string checklist_A_Level, string checklist_degree_diploma, string checklist_certificate, 
            string checklist_transcript, string checklist_birth_certificate, string checklist_other_certificates, string programme_level, 
            string programme_1_name, string programme_1_option_1, string programme_1_option_2, string programme_2_name, 
            string programme_2_option_1, string programme_2_option_2, string programme_3_name, string programme_3_option_1, string programme_3_option_2)
        {
        	this.id = id;
            this.regno = regno;
            this.title = title;
            this.firstname = firstname;
            this.surname = surname;
            this.previous_surname = previous_surname;
            this.dob = dob;
            this.place_of_birth = place_of_birth;
            this.national_id = national_id;
            this.citizenship = citizenship;
            this.postal_address = postal_address;
            this.isResident = isResident;
            this.permit = permit;
            this.period_of_residence = period_of_residence;
            this.passport_no = passport_no;

            this.passport_place_of_issue = passport_place_of_issue;
            this.needs_residence = needs_residence;
            this.disabilities = disabilities;
            this.sex = sex;
            this.phone = phone;
            this.email = email;
            this.next_of_kin_firstname = next_of_kin_firstname;

            this.next_of_kin_surname = next_of_kin_surname;
            this.next_of_kin_postal_address = next_of_kin_postal_address;
            this.next_of_kin_phone = next_of_kin_phone;

            this.next_of_kin_email = next_of_kin_email;
            this.emp_current_employer = emp_current_employer;
            this.emp_notice_period = emp_notice_period;

            this.emp_reference_1 = emp_reference_1;
            this.emp_reference_2 = emp_reference_2;
            this.sponsor_name = sponsor_name;

            this.sponsor_isStaffDependant = sponsor_isStaffDependant;
            this.sponsor_isStaff = sponsor_isStaff;
            this.sponsor_staff_department = sponsor_staff_department;
            this.sponsor_staff_employee_number = sponsor_staff_employee_number;
            this.checklist_identification = checklist_identification;
            this.checklist_5_O_Levels = checklist_5_O_Levels;
            this.checklist_English = checklist_English;
            this.checklist_Mathematics = checklist_Mathematics;
            this.checklist_A_Level = checklist_A_Level;
            this.checklist_degree_diploma = checklist_degree_diploma;
            this.checklist_certificate = checklist_certificate;
            this.checklist_transcript = checklist_transcript;
            this.checklist_birth_certificate = checklist_birth_certificate;
            this.checklist_other_certificates = checklist_other_certificates;
            this.programme_level = programme_level;

            this.programme_1_name = programme_1_name;
            this.programme_1_option_1 = programme_1_option_1;
            this.programme_1_option_2 = programme_1_option_2;
            this.programme_2_name = programme_2_name;
            this.programme_2_option_1 = programme_2_option_1;
            this.programme_2_option_2 = programme_2_option_2;
            this.programme_3_name = programme_3_name;

            this.programme_3_option_1 = programme_3_option_1;
            this.programme_3_option_2 = programme_3_option_2;

        }

        public Register()
        {
        }


        internal static bool saveRegistration(int id, string regno, string title, string firstname, string surname, string previous_surname, string dob,
            string place_of_birth, string national_id, string citizenship, string postal_address, string isResident, string permit,
            string period_of_residence, string passport_no, string passport_place_of_issue, string needs_residence, string disabilities,
            string sex, string phone, string email, string next_of_kin_firstname, string next_of_kin_surname, string next_of_kin_postal_address,
            string next_of_kin_phone, string next_of_kin_email, string emp_current_employer, string emp_notice_period, string emp_reference_1,
            string emp_reference_2, string sponsor_name, string sponsor_isStaffDependant, string sponsor_isStaff, string sponsor_staff_department,
            string sponsor_staff_employee_number, string checklist_identification, string checklist_5_O_Levels, string checklist_English,
            string checklist_Mathematics, string checklist_A_level, string checklist_degree_diploma, string checklist_certificate,
            string checklist_transcript, string checklist_birth_certificate, string checklist_other_certificates, string programme_level,
            string programme_1_name, string programme_1_option_1, string programme_1_option_2, string programme_2_name,
            string programme_2_option_1, string programme_2_option_2, string programme_3_name, string programme_3_option_1, string programme_3_option_2)
        {
            string sql = "";

            SqlCeConnection conn = DB.getConnection();
            if (conn != null)
            {
                try
                {
                    if (id == 0)
                    {
                        sql = "insert into tblStudentDetails (regno, title, firstname, surname, previous_surname, dob, place_of_birth, national_id, citizenship, postal_address, isResident, permit, period_of_residence, passport_no, passport_place_of_issue, needs_residence,disabilities, sex, phone, email, next_of_kin_firstname, next_of_kin_surname, next_of_kin_postal_address, next_of_kin_phone, next_of_kin_email, emp_current_employer, emp_notice_period, emp_reference_1, emp_reference_2, sponsor_name, sponsor_isStaffDependant, sponsor_isStaff, sponsor_staff_department, sponsor_staff_employee_number, checklist_identification, checklist_5_O_Levels, checklist_English, checklist_Mathematics, checklist_A_level, checklist_degree_diploma, checklist_certificate, checklist_transcript, checklist_birth_certificate, checklist_other_certificates, programme_level, programme_1_name, programme_1_option_1, programme_1_option_2, programme_2_name, programme_2_option_1, programme_2_option_2, programme_3_name, programme_3_option_1, programme_3_option_2 ) " +
                         "values (@regno, @title, @firstname, @surname, @previous_surname, @dob, @place_of_birth, @national_id, @citizenship, @postal_address, @isResident, @permit, @period_of_residence, @passport_no, @passport_place_of_issue, @needs_residence, @disabilities, @sex, @phone, @email, @next_of_kin_firstname, @next_of_kin_surname, @next_of_kin_postal_address, @next_of_kin_phone, @next_of_kin_email, @emp_current_employer, @emp_notice_period, @emp_reference_1, @emp_reference_2, @sponsor_name, @sponsor_isStaffDependant, @sponsor_isStaff, @sponsor_staff_department, @sponsor_staff_employee_number, @checklist_identification, @checklist_5_O_Levels, @checklist_English, @checklist_Mathematics, @checklist_A_level, @checklist_degree_diploma, @checklist_certificate, @checklist_transcript, @checklist_birth_certificate, @checklist_other_certificates, @programme_level, @programme_1_name, @programme_1_option_1, @programme_1_option_2, @programme_2_name, @programme_2_option_1, @programme_2_option_2, @programme_3_name, @programme_3_option_1, @programme_3_option_2 )";
                    }
                    else
                    {
                        sql = "update tblStudentDetails set " +
                              " regno=@regno, title=@title, firstname=@firstname, surname=@surname, previous_surname=@previous_surname, dob=@dob, place_of_birth=@place_of_birth, national_id=@national_id, citizenship=@citizenship, postal_address=@postal_address, isResident=@isResident, permit=@permit, period_of_residence=@period_of_residence, passport_no=@passport_no, passport_place_of_issue=@passport_place_of_issue, needs_residence=@needs_residence, disabilities=@disabilities, sex=@sex, phone=@phone, email=@email, next_of_kin_firstname=@next_of_kin_firstname, next_of_kin_surname=@next_of_kin_surname, next_of_kin_postal_address=@next_of_kin_postal_address, next_of_kin_phone=@next_of_kin_phone, next_of_kin_email=@next_of_kin_email, emp_current_employer=@emp_current_employer, emp_notice_period=@emp_notice_period, emp_reference_1=@emp_reference_1, emp_reference_2=@emp_reference_2, sponsor_name=@sponsor_name, sponsor_isStaffDependant=@sponsor_isStaffDependant, sponsor_isStaff=@sponsor_isStaff, sponsor_staff_department=@sponsor_staff_department, sponsor_staff_employee_number=@sponsor_staff_employee_number, checklist_identification=@checklist_identification, checklist_5_O_Levels=@checklist_5_O_Levels, checklist_English=@checklist_English, checklist_Mathematics=@checklist_Mathematics, checklist_A_level=@checklist_A_level, checklist_degree_diploma=@checklist_degree_diploma, checklist_certificate=@checklist_certificate, checklist_transcript=@checklist_transcript, checklist_birth_certificate=@checklist_birth_certificate, checklist_other_certificates=@checklist_other_certificates, programme_level=@programme_level, programme_1_name=@programme_1_name, programme_1_option_1=@programme_1_option_1, programme_1_option_2=@programme_1_option_2, programme_2_name=@programme_2_name, programme_2_option_1=@programme_2_option_1, programme_2_option_2=@programme_2_option_2 , programme_3_name=@programme_3_name, programme_3_option_1=@programme_3_option_1, programme_3_option_2=@programme_3_option_2 " +
                              " where id=@id";
                    }
                    SqlCeCommand cmd = new SqlCeCommand(sql, conn);
                    cmd.Parameters.AddWithValue("@regno", regno);
                    cmd.Parameters.AddWithValue("@title", title);
                    cmd.Parameters.AddWithValue("@firstname", firstname);
                    cmd.Parameters.AddWithValue("@surname", surname);
                    cmd.Parameters.AddWithValue("@previous_surname", previous_surname);
                    cmd.Parameters.AddWithValue("@dob", dob);
                    cmd.Parameters.AddWithValue("@place_of_birth", place_of_birth);
                    cmd.Parameters.AddWithValue("@national_id", national_id);
                    cmd.Parameters.AddWithValue("@citizenship", citizenship);
                    cmd.Parameters.AddWithValue("@postal_address", postal_address);
                    cmd.Parameters.AddWithValue("@isResident", isResident);
                    cmd.Parameters.AddWithValue("@permit", permit);
                    cmd.Parameters.AddWithValue("@period_of_residence", period_of_residence);
                    cmd.Parameters.AddWithValue("@passport_no", passport_no);
                    cmd.Parameters.AddWithValue("@passport_place_of_issue", passport_place_of_issue);
                    cmd.Parameters.AddWithValue("@needs_residence", needs_residence);
                    cmd.Parameters.AddWithValue("@disabilities", disabilities);
                    cmd.Parameters.AddWithValue("@sex", sex);
                    cmd.Parameters.AddWithValue("@phone", phone);
                    cmd.Parameters.AddWithValue("@email", email);
                    cmd.Parameters.AddWithValue("@next_of_kin_firstname", next_of_kin_firstname);
                    cmd.Parameters.AddWithValue("@next_of_kin_surname", next_of_kin_surname);
                    cmd.Parameters.AddWithValue("@next_of_kin_postal_address", next_of_kin_postal_address);
                    cmd.Parameters.AddWithValue("@next_of_kin_phone", next_of_kin_phone);
                    cmd.Parameters.AddWithValue("@next_of_kin_email", next_of_kin_email);
                    cmd.Parameters.AddWithValue("@emp_current_employer", emp_current_employer);
                    cmd.Parameters.AddWithValue("@emp_notice_period", emp_notice_period);
                    cmd.Parameters.AddWithValue("@emp_reference_1", emp_reference_1);
                    cmd.Parameters.AddWithValue("@emp_reference_2", emp_reference_2);
                    cmd.Parameters.AddWithValue("@sponsor_name", sponsor_name);
                    cmd.Parameters.AddWithValue("@sponsor_isStaffDependant", sponsor_isStaffDependant);
                    cmd.Parameters.AddWithValue("@sponsor_isStaff", sponsor_isStaff);
                    cmd.Parameters.AddWithValue("@sponsor_staff_department", sponsor_staff_department);
                    cmd.Parameters.AddWithValue("@sponsor_staff_employee_number", sponsor_staff_employee_number);
                    cmd.Parameters.AddWithValue("@checklist_identification", checklist_identification);
                    cmd.Parameters.AddWithValue("@checklist_5_O_Levels", checklist_5_O_Levels);
                    cmd.Parameters.AddWithValue("@checklist_English", checklist_English);
                    cmd.Parameters.AddWithValue("@checklist_Mathematics", checklist_Mathematics);
                    cmd.Parameters.AddWithValue("@checklist_A_level", checklist_A_level);
                    cmd.Parameters.AddWithValue("@checklist_degree_diploma", checklist_degree_diploma);
                    cmd.Parameters.AddWithValue("@checklist_certificate", checklist_certificate);
                    cmd.Parameters.AddWithValue("@checklist_transcript", checklist_transcript);
                    cmd.Parameters.AddWithValue("@checklist_birth_certificate", checklist_birth_certificate);
                    cmd.Parameters.AddWithValue("@checklist_other_certificates", checklist_other_certificates);
                    cmd.Parameters.AddWithValue("@programme_level", programme_level);
                    cmd.Parameters.AddWithValue("@programme_1_name", programme_1_name);
                    cmd.Parameters.AddWithValue("@programme_1_option_1", programme_1_option_1);
                    cmd.Parameters.AddWithValue("@programme_1_option_2", programme_1_option_2);
                    cmd.Parameters.AddWithValue("@programme_2_name", programme_2_name);
                    cmd.Parameters.AddWithValue("@programme_2_option_1", programme_2_option_1);
                    cmd.Parameters.AddWithValue("@programme_2_option_2", programme_2_option_2);
                    cmd.Parameters.AddWithValue("@programme_3_name", programme_3_name);
                    cmd.Parameters.AddWithValue("@programme_3_option_1", programme_3_option_1);
                    cmd.Parameters.AddWithValue("@programme_3_option_2", programme_3_option_2);
                    if (id > 0)
                    {
                        cmd.Parameters.AddWithValue("@id", id);
                    }

                    cmd.ExecuteNonQuery();
                    return true;
                }
                catch (Exception ex)
                {
                    MessageBox.Show("An error ocurred while trying to access the database. " + ex.Message);
                }
            }
            return false;

        }
    }
}
