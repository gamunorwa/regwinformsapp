﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlServerCe;

namespace BinduraRegistration
{
    class DB
    {
        private static SqlCeConnection conn;

        public static SqlCeConnection getConnection()
        {
            if (conn == null)
            {
                try
                {
                    conn = new SqlCeConnection();

                    conn.ConnectionString =
                        "Persist Security Info = False; Data Source = 'RegistrationDB.sdf';" +
                        "Password = 'bindura123'; " +
                        "Max Database Size = 256; Max Buffer Size = 1024";

                    conn.Open();
                }
                catch (Exception ex)
                {
                    MessageBox.Show("An error ocurred while trying to access the database. " + ex.Message);
                }
            }
            return conn;
        }
    }
}
