﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace BinduraRegistration
{
    public partial class frmRegister : Form
    {
        public frmRegister()
        {
            InitializeComponent();
        }

        private void tabPage1_Click(object sender, EventArgs e)
        {

        }

        private void comboBox10_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            var id = 0; 

            String[] tab1text = {
                                    cmbTitle.Text, cmbSex.Text, txtFirstName.Text, 
                                     txtSurname.Text, txtPreviousSurname.Text, txtPlaceOfBirth.Text, 
                                     txtNationalID.Text, txtCitizenship.Text, txtPostalAddress.Text,
                                     cmbResidentStatus.Text, txtEmail.Text, txtPhone.Text
                                 };
            String[] tab2text = { 
                                    txtKinFirstName.Text, txtKinContacts.Text, txtKinEmail.Text,
                                    txtKinPostalAddress.Text, txtKinSurname.Text
                                };
            String[] tab4text = { 
                                    cmbLevel.Text, txtProgram1Name.Text, txtProgram1Option1.Text,txtProgram1Option2.Text,
                                    txtProgram2Name.Text, txtProgram2Option1.Text, txtProgram2Option2.Text,
                                    txtProgram3Name.Text, txtProgram3Option1.Text, txtProgram3Option2.Text
                                };
            String[] tab5text = { 
                                    txtReference1.Text,txtReference2.Text,txtNoticePeriod.Text, txtCurrentEmployer.Text
                                };
            String[] tab6text = {
                                    txtSponsorName.Text, cmbIsStaff.Text, cmbIsStaffDependant.Text
                                };
            String[] tab7text = { 
                                    txtOtherCertificates.Text, cmbHasALevel.Text,cmbHasBirthCertificate.Text,
                                    cmbHasCertificate.Text,cmbHasDiplomaOrDegree.Text,cmbHasEnglish.Text,
                                    cmbHasFiveOLevels.Text,cmbHasIdentification.Text,cmbHasMaths.Text,cmbHasTranscript.Text
                                };
            if (!isValidText(tab1text))
            {
                tabPersonalDetails.SelectTab(0);
                MessageBox.Show("Some required data is missing.");
            }
            else if (!isValidText(tab2text))
            {
                tabPersonalDetails.SelectTab(1);
                MessageBox.Show("Some required data is missing.");
            }
            else if (lvwExams.Items.Count == 0)
            {
                tabPersonalDetails.SelectTab(2);
                MessageBox.Show("The exam qualifications are missing.");
            }
            else if (!isValidText(tab4text))
            {
                tabPersonalDetails.SelectTab(3);
                MessageBox.Show("Some required data is missing.");
            }
            else if (!isValidText(tab5text))
            {
                tabPersonalDetails.SelectTab(4);
                MessageBox.Show("Some required data is missing.");
            }
            else if (!isValidText(tab6text))
            {
                tabPersonalDetails.SelectTab(5);
                MessageBox.Show("Some required data is missing.");
            }
            else if (!isValidText(tab7text))
            {
                tabPersonalDetails.SelectTab(6);
                MessageBox.Show("Some required data is missing.");
            }
            else if (txtId.Text.Trim().Equals(""))
            {
                id = 0;
            }
            else
            {
                id = Int32.Parse(txtId.Text);
            }        

            bool result = Register.saveRegistration(id, "REG001", cmbTitle.Text, txtFirstName.Text,txtSurname.Text,txtPreviousSurname.Text,
                dtpDOB.Value.ToShortDateString(),txtPlaceOfBirth.Text, txtNationalID.Text, txtCitizenship.Text, txtPostalAddress.Text, cmbResidentStatus.Text,
                txtPermit.Text, txtResidencePeriod.Text, txtPassportNo.Text, txtPassportCountry.Text, cmbConsiderForResidence.Text,
                txtDisabilities.Text, cmbSex.Text, txtPhone.Text, txtEmail.Text, txtKinFirstName.Text, txtKinSurname.Text,
                txtKinPostalAddress.Text, txtKinContacts.Text, txtKinEmail.Text, txtEmployer.Text, txtNoticePeriod.Text, txtReference1.Text,
                txtReference2.Text, txtSponsorName.Text, cmbIsStaffDependant.Text, cmbIsStaff.Text, txtStaffDepartment.Text, txtEmployeeNo.Text,
                cmbHasIdentification.Text, cmbHasFiveOLevels.Text, cmbHasEnglish.Text, cmbHasMaths.Text, cmbHasALevel.Text, cmbHasDiplomaOrDegree.Text,
                cmbHasCertificate.Text, cmbHasTranscript.Text, cmbHasBirthCertificate.Text, txtOtherCertificates.Text, cmbLevel.Text, txtProgram1Name.Text, 
                txtProgram1Option1.Text, txtProgram1Option2.Text, txtProgram2Name.Text, txtProgram2Option1.Text, txtProgram2Option2.Text, 
                txtProgram3Name.Text, txtProgram3Option1.Text, txtProgram3Option2.Text);

            if (result != true)
            {
                MessageBox.Show("Oops. The record could not be saved.");
            }



        }

        private bool isValidText(string[] tab1text)
        {
            foreach(String txt in tab1text){
                if(txt.Trim().Equals("")){
                    return false;
                }
            }
            return true;
        }

        private void frmStudentRegistration_Load(object sender, EventArgs e)
        {
            initialiseExamsListView();
            initialiseEmploymentListView();
        }

        private void initialiseExamsListView()
        {
            lvwExams.View = View.Details;

            lvwExams.LabelEdit = true;

            lvwExams.AllowColumnReorder = true;

            lvwExams.FullRowSelect = true;

            lvwExams.GridLines = true;

            lvwExams.Sorting = SortOrder.Ascending;

            lvwExams.Columns.Add("Date", 60, HorizontalAlignment.Left);
            lvwExams.Columns.Add("Board", 100, HorizontalAlignment.Left);
            lvwExams.Columns.Add("Level", 60, HorizontalAlignment.Left);
            lvwExams.Columns.Add("Subject", 120, HorizontalAlignment.Left);
            lvwExams.Columns.Add("Result", 50, HorizontalAlignment.Left);

        }

        private void initialiseEmploymentListView()
        {
            lvwEmployment.View = View.Details;

            lvwEmployment.LabelEdit = true;

            lvwEmployment.AllowColumnReorder = true;

            lvwEmployment.FullRowSelect = true;

            lvwEmployment.GridLines = true;

            lvwEmployment.Sorting = SortOrder.Ascending;

            lvwEmployment.Columns.Add("From", 60, HorizontalAlignment.Left);
            lvwEmployment.Columns.Add("To", 60, HorizontalAlignment.Left);
            lvwEmployment.Columns.Add("Employer/Occupation", 130, HorizontalAlignment.Left);
            lvwEmployment.Columns.Add("Duties", 130, HorizontalAlignment.Left);

        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnEmpAdd_Click(object sender, EventArgs e)
        {
            String[] data = { txtEmployer.Text, txtDuties.Text };
            if (!isValidText(data))
            {
                MessageBox.Show("Some required data is missing.");
            }
            else
            {
                ListViewItem lvi = new ListViewItem(dtpEmpFrom.Value.ToString());
                lvi.SubItems.Add(dtpEmpTo.Value.ToString());
                lvi.SubItems.Add(txtEmployer.Text.ToString());
                lvi.SubItems.Add(txtDuties.Text.ToString());

                // Add the list items to the ListView
                lvwEmployment.Items.Add(lvi);

                dtpEmpFrom.Value = DateTime.Now;
                dtpEmpTo.Value = DateTime.Now;
                txtEmployer.Text = "";
                txtDuties.Text = "";

            }
        }

        private void btnEmpRemove_Click(object sender, EventArgs e)
        {
            if (lvwEmployment.SelectedItems.Count > 0)
            {
                lvwEmployment.SelectedItems[0].Remove();
            }
        }

        private void btmExamAdd_Click(object sender, EventArgs e)
        {
            String[] data = { txtExamDate.Text, txtExamBoard.Text, txtExamLevel.Text, txtExamSubject.Text, txtExamResult.Text };
            if (!isValidText(data))
            {
                MessageBox.Show("Some required data is missing.");
            }
            else
            {
                ListViewItem lvi = new ListViewItem(txtExamDate.Text);
                lvi.SubItems.Add(txtExamBoard.Text);
                lvi.SubItems.Add(txtExamLevel.Text);
                lvi.SubItems.Add(txtExamSubject.Text);
                lvi.SubItems.Add(txtExamResult.Text);

                // Add the list items to the ListView
                lvwExams.Items.Add(lvi);

                txtExamSubject.Text = "";
                txtExamResult.Text = "";
                txtExamSubject.Focus();
            }
        }

        private void btnExamRemove_Click(object sender, EventArgs e)
        {
            if (lvwExams.SelectedItems.Count > 0)
            {
                lvwExams.SelectedItems[0].Remove();
            }
        }

        private void btnClearExam_Click(object sender, EventArgs e)
        {
            txtExamDate.Text = "";
            txtExamBoard.Text = "";
            txtExamLevel.Text = "";
            txtExamSubject.Text = "";
            txtExamResult.Text = "";
        }


        
    }
}
