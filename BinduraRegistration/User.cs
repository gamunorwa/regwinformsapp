﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlServerCe;
using System.Data;
using System.Windows.Forms;
using System.Security.Cryptography;

namespace BinduraRegistration
{
    class User
    {
        public String username;
        public String passwordHash;

        public User()
        {
        }

        public User(Int32 id, String username, String passwordHash)
        {
            this.username = username;
            this.passwordHash = passwordHash;
        }

        public static User auth(String username, String password)
        {
            var passwordHash = password; // EncodePasswordToBase64(password);
            SqlCeConnection conn = DB.getConnection();
            if (conn != null)
            {
                try
                {
                    SqlCeDataAdapter sda = new SqlCeDataAdapter("Select id, username, password from tblUsers where username=@username and password=@password", conn);

                    sda.SelectCommand.Parameters.AddWithValue("@username", username);
                    sda.SelectCommand.Parameters.AddWithValue("@password", passwordHash);

                    DataTable dt = new DataTable();
                    sda.Fill(dt);
                    if (dt.Rows.Count == 1)
                    {
                        return new User(
                            Int32.Parse(dt.Rows[0][0].ToString()),
                            dt.Rows[0][1].ToString(),
                            dt.Rows[0][2].ToString()
                        );

                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("An error ocurred while trying to access the database. " + ex.Message);
                }
            }

            return new User();
        }

        public static string EncodePasswordToBase64(string password)
        {
            byte[] bytes = Encoding.Unicode.GetBytes(password);
            byte[] inArray = HashAlgorithm.Create("SHA1").ComputeHash(bytes);
            return Convert.ToBase64String(inArray);
        }

        public static DataTable getUsers(string sql)
        {
            SqlCeConnection conn = DB.getConnection();
            if (conn != null)
            {
                try
                {
                    SqlCeDataAdapter sda = new SqlCeDataAdapter(sql, conn);
                    DataTable dt = new DataTable();
                    sda.Fill(dt);
                    return dt;
                }
                catch (Exception ex)
                {
                    MessageBox.Show("An error ocurred while trying to access the database. " + ex.Message);
                }
            }
            return new DataTable();
        }

        internal static User findById(int id)
        {
            SqlCeConnection conn = DB.getConnection();
            if (conn != null)
            {
                try
                {
                    SqlCeDataAdapter sda = new SqlCeDataAdapter("Select id, username, password from tblUsers where id=@id", conn);

                    sda.SelectCommand.Parameters.AddWithValue("@id", id);

                    DataTable dt = new DataTable();
                    sda.Fill(dt);
                    if (dt.Rows.Count == 1)
                    {
                        return new User(
                            Int32.Parse(dt.Rows[0][0].ToString()),
                            dt.Rows[0][1].ToString(),
                            dt.Rows[0][2].ToString()
                        );

                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("An error ocurred while trying to access the database. " + ex.Message);
                }
            }

            return new User();
        }


        internal static User findByUsername(string username)
        {
            SqlCeConnection conn = DB.getConnection();
            if (conn != null)
            {
                try
                {
                    SqlCeDataAdapter sda = new SqlCeDataAdapter("Select id, username, password from tblUsers where username=@username", conn);

                    sda.SelectCommand.Parameters.AddWithValue("@username", username);

                    DataTable dt = new DataTable();
                    sda.Fill(dt);
                    if (dt.Rows.Count == 1)
                    {
                        return new User(
                            Int32.Parse(dt.Rows[0][0].ToString()),
                            dt.Rows[0][1].ToString(),
                            dt.Rows[0][2].ToString()
                        );

                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("An error ocurred while trying to access the database. " + ex.Message);
                }
            }

            return new User();
        }

 
    }
}
